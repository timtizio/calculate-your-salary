package org.cda.timothe.calculateyoursalary;

import org.cda.timothe.calculateyoursalary.dao.DAOFactory;
import org.cda.timothe.calculateyoursalary.dao.implementations.ResultDAOImpl;
import org.cda.timothe.calculateyoursalary.dao.interfaces.DAO;
import org.cda.timothe.calculateyoursalary.dao.interfaces.KeyValueDAO;
import org.cda.timothe.calculateyoursalary.models.MetaData;
import org.cda.timothe.calculateyoursalary.models.Result;
import org.junit.Test;

import static org.junit.Assert.*;

public class DAOFactoryTest {

    /**
     * We just test that DAOFactory.getIntance() don't return null
     */
    @Test
    public void instantiateDAOFactory() {
        DAOFactory factory = DAOFactory.getInstance();
        assertNotNull( factory );
    }

    /**
     * We just test that getResultDAO() don't return null
     */
    @Test
    public void instantiateResultDAO() {
        DAO<Result> rdao = DAOFactory.getInstance().getResultDAO();
        assertNotNull( rdao );
    }

    /**
     * We just test that getMetaDataDAO() don't return null
     */
    @Test
    public void instantiateMetaDataDAO() {
        KeyValueDAO<MetaData> mdao = DAOFactory.getInstance().getMetaDataDAO();
        assertNotNull( mdao );
    }

}
