package org.cda.timothe.calculateyoursalary.dao.interfaces;

import android.database.SQLException;

/**
 * The key-value DAO interface. It's an extension of DAO interface.
 * <p>This interface allows a finding by a key.</p>
 *
 * @param <T> The type of object that is input and output of this class
 * @see DAO
 *
 * @author Timothe Tizio-Menut
 */
public interface KeyValueDAO<T> extends DAO<T> {

    /**
     * Find an object with his key.
     * <p>If the key don't exist in database, throw an exception.</p>
     *
     * @param key the entity key to find
     * @return the entity found
     * @throws Exception if the key is not found
     * @throws SQLException if database connection failed
     */
    T find(String key) throws Exception, SQLException;

}
