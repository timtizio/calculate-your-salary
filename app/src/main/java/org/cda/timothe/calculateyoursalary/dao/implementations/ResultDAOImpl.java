package org.cda.timothe.calculateyoursalary.dao.implementations;

import android.database.SQLException;

import org.cda.timothe.calculateyoursalary.dao.interfaces.DAO;
import org.cda.timothe.calculateyoursalary.models.Result;

import java.util.List;

/**
 * This class is an DAO implementation to result models.
 *
 * @see org.cda.timothe.calculateyoursalary.dao.DAOFactory
 * @see DAO
 *
 * @author Timothe Tizio-Menut
 */
public class ResultDAOImpl implements DAO<Result> {

    @Override
    public boolean create(Result obj) throws Exception, SQLException {
        return false;
    }

    @Override
    public Result find(int ID) throws Exception, SQLException {
        return null;
    }

    @Override
    public List<Result> all() throws SQLException {
        return null;
    }

    @Override
    public boolean update(Result obj) throws Exception, SQLException {
        return false;
    }

    @Override
    public boolean delete(Result obj) throws Exception, SQLException {
        return false;
    }
}
