package org.cda.timothe.calculateyoursalary.dao.interfaces;

import android.database.SQLException;
import java.util.List;

/**
 * The DAO interface.
 *
 * @param <T> The type of object that is input and output of this class
 *
 * @author Timothe Tizio-Menut
 */
public interface DAO<T> {

    /**
     * Store a new T instance.
     * <p>T should not have an ID. If he has one, this method throw an exception</p>
     *
     * @param obj Entity to create
     * @return true if success, else return false
     * @throws Exception if the obj is already in database
     * @throws SQLException if database connection failed
     */
    boolean create(T obj) throws Exception, SQLException;

    /**
     * Find an object with his Id
     * <p>If the ID don't exist in database, throw an exception</p>
     *
     * @param ID The entity id to find
     * @return The entity found
     * @throws Exception if the obj is not found
     * @throws SQLException if database connection failed
     */
    T find(int ID) throws Exception, SQLException;

    /**
     * Find all occurrence of entity in database
     *
     * @return List of all occurrence
     * @throws SQLException if database connection failed
     */
    List<T> all() throws SQLException;

    /**
     * Update the entry in databse.
     * <p>The ID should be set and exist in database. If the ID is not found in database, throw an exception.</p>
     *
     * @param obj entity to update
     * @return true if success, else return false
     * @throws Exception if the obj is not found
     * @throws SQLException if database connection failed
     */
    boolean update(T obj) throws Exception, SQLException;

    /**
     * Delete the entry.
     * <p>The ID should be set and exist in database. If the ID is not found in database, throw an exception.</p>
     *
     * @param obj entity to delete
     * @return true if success, else return false
     * @throws Exception if the obj is not found
     * @throws SQLException if database connection failed
     */
    boolean delete(T obj) throws Exception, SQLException;

}
