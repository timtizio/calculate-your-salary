package org.cda.timothe.calculateyoursalary.models;

/**
 * A MetaData can be a calculation parameter or a revenue/expense
 *
 * @author Timothe Tizio-Menut
 */
public class MetaData {

    /**
     * The database primary key, it's unsigned.
     * <p>It can be null if this MetaData is not in DB.</p>
     */
    private int ID;

    /**
     * The key of this MetaData
     * <p><b>Warning : Never update a parameter key</b>. The key was used to retrieve the parameter value in database.</p>
     */
    private String key;

    /**
     * The value of this MetaData
     * <p>If this MetaData is an expense, value is negative</p>
     */
    private float value;

    /**
     * This attribute allows to know easily if this MetaData is a parameter or not.
     * <p>If this value is false : this MetaData is either revenue (if his value is positive), or expense (if his value is negative)</p>
     * <p>If this value is true : this MetaData is a calculation parameter</p>
     */
    private boolean isParameter;

    public MetaData(int ID, String key, float value, boolean isParameter) {
        this.ID = ID;
        this.key = key;
        this.value = value;
        this.isParameter = isParameter;
    }

    public MetaData(String key, float value, boolean isParameter) {
        this.ID = ID;
        this.key = key;
        this.value = value;
        this.isParameter = isParameter;
    }

    public int getID() {
        return ID;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public boolean isParameter() {
        return isParameter;
    }

    public void setParameter(boolean parameter) {
        isParameter = parameter;
    }
}
