package org.cda.timothe.calculateyoursalary.dao;

import org.cda.timothe.calculateyoursalary.dao.implementations.MetaDataDAOImpl;
import org.cda.timothe.calculateyoursalary.dao.implementations.ResultDAOImpl;
import org.cda.timothe.calculateyoursalary.dao.interfaces.DAO;
import org.cda.timothe.calculateyoursalary.dao.interfaces.KeyValueDAO;
import org.cda.timothe.calculateyoursalary.models.MetaData;
import org.cda.timothe.calculateyoursalary.models.Result;

/**
 * This class is factory for each DAO implementation.
 * <p>It's a singleton. To instantiate it, you should be call his static method : getInstance().</p>
 *
 * @author Timothe Tizio-Menut
 */
public class DAOFactory {

    /**
     * The unique instance of this class.
     */
    private static DAOFactory instance = null;

    /**
     * The unique instance of resultDAOImpl.
     */
    private DAO<Result> resultDAOInstance = null;

    /**
     * The unique instance of MetaDataDAOImpl.
     */
    private KeyValueDAO<MetaData> metaDataDAOInstance = null;

    /**
     * The private constructor of this class.
     */
    private DAOFactory(){
        /*
         * TODO: implement this method
         */
    }

    /**
     * The singleton implentation.
     * <p>Is the unique way to get an instance of this class.</p>
     *
     * @return the instance
     */
    public static DAOFactory getInstance() {
        if( null == instance )
            instance = new DAOFactory();

        return instance;
    }

    /**
     * The getter of resultDAO.
     * <p>This way allows to instantiate an unique instance of this DAO.</p>
     * <p>This method check just if an instance of ResultDAO is create. If is not the case, creating the instance. Finally, return the instance.</p>
     *
     * @return the ResultDAO instance
     */
    public DAO<Result> getResultDAO() {
        if( null == resultDAOInstance )
            resultDAOInstance = new ResultDAOImpl();

        return resultDAOInstance;
    }

    /**
     * The getter of MetaDataDAO.
     * <p>This way allows to instantiate an unique instance of this DAO.</p>
     * <p>This method check just if an instance of MetaDataDAO is create. If is not the case, creating the instance. Finally, return the instance.</p>
     *
     * @return the MetaDataDAO instance
     */
    public KeyValueDAO<MetaData> getMetaDataDAO() {
        if( null == metaDataDAOInstance )
            metaDataDAOInstance = new MetaDataDAOImpl();
        return metaDataDAOInstance;
    }



}
