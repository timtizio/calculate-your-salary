package org.cda.timothe.calculateyoursalary.dao.implementations;

import android.database.SQLException;

import org.cda.timothe.calculateyoursalary.dao.interfaces.KeyValueDAO;
import org.cda.timothe.calculateyoursalary.models.MetaData;

import java.util.List;

/**
 * This class is an DAO implementation to MetaData models.
 *
 * @see org.cda.timothe.calculateyoursalary.dao.DAOFactory
 * @see KeyValueDAO
 * @see org.cda.timothe.calculateyoursalary.dao.interfaces.DAO
 *
 * @author Timothe Tizio-Menut
 */
public class MetaDataDAOImpl implements KeyValueDAO<MetaData> {



    @Override
    public boolean create(MetaData obj) throws Exception, SQLException {
        return false;
    }

    @Override
    public MetaData find(String key) throws Exception, SQLException {
        return null;
    }

    @Override
    public MetaData find(int ID) throws Exception, SQLException {
        return null;
    }

    @Override
    public List<MetaData> all() throws SQLException {
        return null;
    }

    @Override
    public boolean update(MetaData obj) throws Exception, SQLException {
        return false;
    }

    @Override
    public boolean delete(MetaData obj) throws Exception, SQLException {
        return false;
    }
}
