package org.cda.timothe.calculateyoursalary.models;

import java.util.Date;

/**
 * A result is all data of a salary.
 *
 * @author Timothe Tizio-Menut
 */
public class Result {

    /**
     * The database primary key, it's unsigned.
     * <p>It can be null if this result is not in DB.</p>
     */
    private int ID;

    /**
     * The salary with every gross (that of the salaried and that of company).
     * <p>It's the biggest of all values.</p>
     */
    private float salaryTotalCompany;

    /**
     * The salary with only salaried gross.
     * <p>Generally, it's the value provided by the employer.</p>
     */
    private float grossSalary;

    /**
     * The net salary received by the salaried.
     * <p>It's the net salary <b>before tax</b>.</p>
     */
    private float netSalary;

    /**
     * The salary after tax.
     * <p>That's what's left after the collection at the source.</p>
     */
    private float taxFreeNetSalary;

    /**
     * That's money's left after paid every expense and after received every revenues.
     * <p>This value can be the same at the taxFreeNetSalary if there is no expense and revenues.</p>
     */
    private float moneyAfterPaid;

    /**
     * The first value set by the user.
     * <p>This allows the recalculate salaries values if the parameters are changed.</p>
     */
    private enum initialValue{
        salaryTotalCompany,
        grossSalary,
        netSalary,
        taxFreeNetSalary
    }

    /**
     * The label of this result
     * <p>This allows the user to easily find the result in the results history.</p>
     */
    private String label;

    /**
     * The date of the adding this result
     */
    private Date date;

    /**
     *
     * @return current ID of this result. Return -1 if is not in database
     */
    public int getID() {
        return ID;
    }

    /**
     *
     * @return current salaryTotalCompany
     */
    public float getSalaryTotalCompany() {
        return salaryTotalCompany;
    }

    /**
     *
     * @param salaryTotalCompany salaryTotalCompany to set
     */
    public void setSalaryTotalCompany(float salaryTotalCompany) {
        this.salaryTotalCompany = salaryTotalCompany;
    }

    /**
     *
     * @return current grossSalary
     */
    public float getGrossSalary() {
        return grossSalary;
    }

    /**
     *
     * @param grossSalary grossSalary to set
     */
    public void setGrossSalary(float grossSalary) {
        this.grossSalary = grossSalary;
    }

    /**
     *
     * @return current netSalary
     */
    public float getNetSalary() {
        return netSalary;
    }

    /**
     *
     * @param netSalary netSalary to set
     */
    public void setNetSalary(float netSalary) {
        this.netSalary = netSalary;
    }

    /**
     *
     * @return current taxFreeNetSalary
     */
    public float getTaxFreeNetSalary() {
        return taxFreeNetSalary;
    }

    /**
     *
     * @param taxFreeNetSalary taxFreeNetSalary to set
     */
    public void setTaxFreeNetSalary(float taxFreeNetSalary) {
        this.taxFreeNetSalary = taxFreeNetSalary;
    }

    /**
     *
     * @return current moneyAfterPaid
     */
    public float getMoneyAfterPaid() {
        return moneyAfterPaid;
    }

    /**
     *
     * @param moneyAfterPaid moneyAfterPaid to set
     */
    public void setMoneyAfterPaid(float moneyAfterPaid) {
        this.moneyAfterPaid = moneyAfterPaid;
    }

    /**
     *
     * @return current label
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return current date
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param date date to set
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * This method is to calculate every salaries values.
     * <p>Be careful when recalculating if the parameters are changed, because all current values will be lost</p>
     */
    public void calculate() {
        /*
         * TODO: implement this method
         */
    }

}
